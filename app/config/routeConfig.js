module.exports = app => {
    require("../routes/authRoute")(app);
    require("../routes/dashboardRoute")(app);
    require("../routes/adminRoute")(app);
    // require("../routes/userRoute")(app);
    require("../routes/systemEmailRoute")(app);
    require("../routes/organizationRoute")(app);
    require("../routes/electionRoute")(app);
    require("../routes/ballotRoute")(app);
    require("../routes/settingRoute")(app);
    require("../routes/voterRoute")(app);
    // require("../routes/chatRoute")(app)
    
    app.all('*',(req,res) => {
        res.render('../view/error/404.handlebars',{})
    })
}