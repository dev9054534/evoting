const mongoose = require('mongoose');

const voterSchema = new mongoose.Schema({
    iUserId: {
        type: String,
    },
    voters: {
        Index: {
            type: String
        },
        Email: {
            type: String
        },
        Phone: {
            type: Number
        }
    },
});

const voterData = mongoose.model('voters', voterSchema);

module.exports = voterData;