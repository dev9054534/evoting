const mongoose = require('mongoose');

const LangModel = new mongoose.Schema({
    vLangTitle: {
        type: String
    },
    vLangCode: {
        type: String
    },
    eLangStatus: {
        type: String,
        enum: ['Active', 'Inactive']
    }
})

module.exports = mongoose.model('languages', LangModel);