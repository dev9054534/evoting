const mongoose = require('mongoose');

const electionModel = new mongoose.Schema({
    vVoteTitle: {
        type: String
    },
    dtStart: {
        type: String
    },
    dtEnd: {
        type: String
    },
    vBallotTitle: {
        type: String
    },
    tVotingInstruction: {
        type: String
    },
    iCandidate: {
        type: String
    },
    aCandidateList: {
        type: String
    },
    eEmailNotice:{
        type: String,
        enum: ['Yes', 'No']
    },
    eSmsNotice:{
        type: String,
        enum: ['Yes', 'No']
    },
})

module.exports = mongoose.model('elections', electionModel);