const mongoose = require('mongoose');

const LangLabelModel = new mongoose.Schema({
    vLangLabel: {
        type: String
    },
    vLangTitle: {
        type: String
    },
    eStatus: {
        type: String,
        enum: ['Active', 'Inactive']
    }
})

module.exports= mongoose.model('language_labels', LangLabelModel)