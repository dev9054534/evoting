const mongoose = require('mongoose');
const validator = require('validator');
const bcrypt = require('bcrypt');

const adminSchema = new mongoose.Schema({
    username: {
        type: String,
    },
    email: {
        type: String,
        required: [true, "Email is a required field"],
        validate(value) {
            if (!validator.isEmail(value)) {
                throw new Error("Please enter a valid E-mail!");
            }
        }
    },
    image: {
        type: String
    },
    eType: {
        type: String,
        enum: ['admin', 'organizer'],
    },

    phone: {
        type: Number
    },
    password: {
        type: String,
        required: [true, "Password is a required field"],
        select: false, // Password won't be included by default when querying
        validate(value) {
            if (!validator.isLength(value, { min: 6, max: 15 })) {
                throw Error("Password length should be between 6 and 15 characters");
            }
            if (!(/[A-Z]/.test(value) && /[^a-zA-Z\d]/.test(value))) {
                throw new Error("Password must contain at least one uppercase letter and one special character");
            }

        }
    },
    status: {
        type: String,
        enum: ['Active', 'Inactive'],
        default: 'Active' // Set a default value if needed
    },
    organization_details: {
        o_name: {
            type: String,
        },
        o_location: {
            type: String
        },
        o_industry: {
            type: String
        },
        o_membersize: {
            type: String
        },
    },
}, {
    timestamps: true,  // Automatically add createdAt and updatedAt timestamps
});

// Middleware: Pre-save hook to hash the password before saving to the database
adminSchema.pre('save', async function (next) {
    // Only hash the password if it's modified or new (not on updates where password isn't changed)
    if (!this.isModified('password')) {
        return next();
    }

    try {
        // Generate a salt and hash the password
        const salt = await bcrypt.genSalt(10);
        const hashedPassword = await bcrypt.hash(this.password, salt);

        // Store the hashed password
        this.password = hashedPassword;
        next();
    } catch (error) {
        console.log('error: ', error);
        return next(error);
    }
});

const User = mongoose.model('admin', adminSchema);

module.exports = User;
