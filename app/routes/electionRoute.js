module.exports = (app) => {
    const router = require("express").Router();
    const electionRoute = require("../controller/electionController");
  
    router.get("/", electionRoute.index);
    router.get('/add', electionRoute.add);
    router.get('/get-data', electionRoute.getData);
  
    app.use("/election", router);
  };
  