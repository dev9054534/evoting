const { Router } = require('express');

module.exports = (app) =>{
    const router = require('express').Router();
    const SettingController = require('../controller/settingController');

    router.get('/setting', SettingController.index);
    router.get('/email-setting', SettingController.emailSetting);
    router.get('/social-setting', SettingController.socialSetting);
    router.get('/company-info', SettingController.companyInfo);
    router.post('/store-data', SettingController.storeData);
    router.get('/language-setting', SettingController.languageSetting);
    router.post('/ajax_listing', SettingController.ajax_listing);
    router.get('/add', SettingController.add);
    router.post('/lang-data-store', SettingController.langDataStore);
    router.get('/generate-label', SettingController.generateLabel);
    router.get('/edit/:id', SettingController.edit);


    app.use('/general-setting', router)
}