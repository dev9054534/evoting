module.exports = (app) => {
    const router = require("express").Router();
    const voterController = require('../controller/voterController');

    router.get('/', voterController.index);
    router.post("/ajax_listing", voterController.ajax_listing);
    router.post('/save-csv', voterController.save_voter_csv);
    router.get('/add', voterController.add);
    router.post('/store-data', voterController.storeData);
    router.get('/edit/:id', voterController.edit);


    app.use("/voters", router);
}