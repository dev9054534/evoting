module.exports = (app) =>{
    const router = require("express").Router();
    const organization = require('../controller/organizationController');

    router.get('/', organization.index);
    router.post('/ajax_listing', organization.ajax_listing);
    router.get('/add', organization.add);
    router.post('/add-action', organization.addAction);
    router.get('/edit/:id', organization.edit);


    app.use("/organizations", router);
}