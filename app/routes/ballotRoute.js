module.exports = (app) => {
    const router = require('express').Router();
    const ballotController = require('../controller/ballotController');

    router.get('/', ballotController.index);

    app.use('/ballot', router)
}