// This middleware function is responsible for handling user permissions based on session data.
module.exports = (req, res, next) => {
    if (req.session.email) {
        const userPermisson = req.session.eType;
    
        if (userPermisson === "organization") {
            return res.render('../view/error/403.handlebars',{})
        }
          // If the user is admin , proceed to the next middleware.
        next();
    } else {
        return res.redirect('/')
    }
}
