const SettingModel = require('../model/settingModel');
const path = require('path');
const location = path.join(__dirname + '/../');
const fs = require('fs');
const labelModel = require('../model/languageLabelModel');
const Paginator = require('../library/PaginatorLibrary')

exports.index = async (req, res) => {
    if (req.session.email) {
        try {
            const data = await SettingModel.find();
            res.render("../view/setting/generalSetting", { is_layout: true, data: data[0], eType: req.session.eType });
        } catch (error) {
            console.error('error: ', error);
            req.flash("error", "An error occurred.");
            res.redirect("/dashboard");
        }
    } else {
        res.redirect('/');
    }

}

exports.emailSetting = async (req, res) => {
    if (req.session.email) {
        try {
            const data = await SettingModel.find();
            res.render("../view/setting/emailSetting", { is_layout: true, data: data[0], eType: req.session.eType  })
        } catch (error) {
            console.error('error: ', error);
            req.flash("error", "An error occurred.");
            res.redirect("/dashboard");
        }
    } else {
        res.redirect('/');
    }
}

exports.socialSetting = async (req, res) => {
    if (req.session.email) {
        try {
            const data = await SettingModel.find();
            res.render("../view/setting/socialSetting", { is_layout: true, data: data[0], eType: req.session.eType  });
        } catch (error) {
            console.error('error: ', error);
            req.flash("error", "An error occurred.");
            res.redirect("/dashboard");
        }
    } else {
        res.redirect('/');
    }

}

exports.companyInfo = async (req, res) => {
    if (req.session.email) {
        try {
            const data = await SettingModel.find();
            res.render("../view/setting/companyInfo", { is_layout: true, data: data[0], eType: req.session.eType});
        } catch (error) {
            console.error('error: ', error);
            req.flash("error", "An error occurred.");
            res.redirect("/dashboard");
        }
    } else {
        res.redirect('/');
    }
}

exports.languageSetting = async (req, res) => {
    if (req.session.email) {
        try {
            res.render("../view/setting/languageIndex", { is_layout: true,eType: req.session.eType});
        } catch (error) {
            console.error('error: ', error);
            req.flash("error", "An error occurred.");
            res.redirect("/dashboard");
        }
    } else {
        res.redirect('/');
    }
}

exports.ajax_listing = async (req, res) => {
    if (req.session.email) {
        try {
          let { iLangId, vAction, searchInput, vPage } = req.body;
    
          if (
            vAction === "search" ||
            vAction === "delete" ||
            vAction === "multiple_delete"
          ) {
            if (vAction === "delete" && iLangId != null) {
              await labelModel.findByIdAndDelete({_id: iLangId});
            }
    
            if (vAction === "multiple_delete" && iLangId != null) {
              let langID = iLangId.split(",");
    
              await labelModel.deleteMany({
                _id: {
                  $in: langID,
                },
              });
            }
    
            let searchSQL = {};
            if (searchInput.length > 0) {
              let generalSearch = new RegExp(searchInput, "i");
              searchSQL.$or = [
                { username: generalSearch },
                { email: generalSearch },
                // { iContact: generalSearch },
                { status: new RegExp(searchInput) },
              ];
            }
    
            // Pagination
            var companyData = await SettingModel.findOne().exec();
    
            var dataCount = await labelModel.find().count();
            vPage = 1;
            let vItemPerPage = companyData.iItemPerPage || 5;
            let vCount = dataCount;
    
            if (req.body.vPage != "") {
              vPage = req.body.vPage;
            }
    
            let criteria = {
              vPage: vPage,
              vItemPerPage: vItemPerPage,
              vCount: vCount,
            };
    
            let paginator = Paginator.pagination(criteria);
            let start = (vPage - 1) * vItemPerPage;
            let limit = vItemPerPage;
            // End
    
            let languageData = await labelModel.find(searchSQL).skip(start).limit(limit).sort({ _id: "desc" });
    
            res.render("../view/setting/lang_ajax_listing", {
              layout: false,
              languageData: languageData,
              paginator: paginator,
              datalimit: limit,
              dataCount: dataCount,
              iAgentId: req.session.userId,
            });
          }
        } catch (error) {
          console.log("error: ", error);
          return res.status(500).send({
            message: "error!",
          });
        }
      } else {
        res.status(401).send("Unauthorized");
      }
}

exports.storeData = async (req, res) => {

    if (req.session.email) {
        try {
            if (req.files) {
                
                // const uploadLocation = location + "upload/";

                // const moveFile = async (file, newName) => {
                //     console.log('newName: ', newName);
                //     if (!file) return ""

                //     await file.mv(uploadLocation + newName);
                //     return newName;
                // };

                // let companyFaviconName;
                // let companyLogoName;
                // let footerLogoName;
                // if (req.files.companyFavicon) {
                //      companyFaviconName = await moveFile(req.files.companyFavicon, req.files.companyFavicon.name);
                // }

                // if (req.files.companyLogo) {
                //      companyLogoName = await moveFile(req.files.companyLogo, req.files.companyLogo.name);
                // }

                // if (req.files.footerLogo) {
                //      footerLogoName = await moveFile(req.files.footerLogo, req.files.footerLogo.name);
                // }



                if (req.files.companyFavicon) {
                    const companyFavicon = req.files.companyFavicon;
                    companyFavicon.mv(location + "upload/" + req.files.companyFavicon.name);
                    var companyFaviconName = "";
                    companyFaviconName = req.files.companyFavicon.name;
                }

                if (req.files.companyLogo) {
                    const companyLogo = req.files.companyLogo;
                    companyLogo.mv(location + "upload/" + req.files.companyLogo.name);
                    var companyLogoName = "";
                    companyLogoName = req.files.companyLogo.name;
                }

                if (req.files.footerLogo) {
                    const footerLogo = req.files.footerLogo;
                    footerLogo.mv(location + "upload/" + req.files.footerLogo.name);
                    var footerLogoName = "";
                    footerLogoName = req.files.footerLogo.name;
                }
            }
            const response = await SettingModel.find();

            const data = {
                vContrlPanelTitle: req.body.siteTitle,
                vMessageToDisplay: req.body.msgToBeDisplay,
                iItemPerPage: req.body.itemPerPage,
                vCompanyAddress: req.body.companyAddress,
                vCompanyDescription: req.body.companyDescription,
                vCompanyEmailId: req.body.companyEmail,
                vCompanyFavicon: companyFaviconName,
                vCompanyLogo: companyLogoName,
                vCompanyName: req.body.companyName,
                iCompanyNumber: req.body.companyNumber,
                vCompanyWebsite: req.body.companyWebsite,
                vCopyrightText: req.body.copyrightText,
                vFooterLogo: footerLogoName,
                vAdminEmailAddress: req.body.adminEmail,
                vEmailProtocol: req.body.emailProtocol,
                vSMTPServerHost: req.body.serverHost,
                vSMTPServerPassword: req.body.serverPass,
                iSMTPServerPort: req.body.serverPort,
                vSMTPServerUsername: req.body.serverUsername,
                vSupportEmail: req.body.supportEmail,
                vFacebookUrl: req.body.fbUrl,
                vInstagramUrl: req.body.instaUrl,
                vLinkedinUrl: req.body.linkedinUrl,
                vPinterestUrl: req.body.pinterestUrl,
                vTwitterUrl: req.body.twitterUrl,
                vYoutubeUrl: req.body.youtubeUrl,
            }

            if (response.length == 0) {
                // await new SettingModel(data).save();
                const newSettings = new SettingModel(data);
                await newSettings.save();
            } else {
                // await SettingModel.updateOne({ _id: response[0]._id, $set: data });
                const existingSettingsId = response[0]._id;
                await SettingModel.updateOne({ _id: existingSettingsId }, { $set: data });
            }
            req.flash("success", "Data stored successfully!!")
            res.redirect('/dashboard');

        } catch (err) {
            console.error(err);
            req.flash("error", "Something Went Wrong");
            res.redirect('/dashboard');
        }
    } else {
        res.redirect('/');
    }
}

exports.add = async (req, res) => {
    if (req.session.email) {
        try {
            res.render("../view/setting/languageAdd", { is_layout: true, action: 'add'});
        } catch (error) {
            console.error('error: ', error);
            req.flash("error", "An error occurred.");
            res.redirect("/dashboard");
        }
    } else {
        res.redirect('/');
    }
}

exports.langDataStore = async (req, res) => {
    if(req.session.email){
        try{
            let {vLangLabel, vLangTitle, eStatus} = req.body;

            const lang_data = {
                vLangLabel,
                vLangTitle,
                eStatus
            }
            if(req.body.action == 'add'){
                await new labelModel(lang_data).save();
                req.flash('success', 'Language label added successfully.');
            }else{
                await labelModel.updateOne({_id: req.body.iLangId}, {$set:lang_data});
                req.flash('success', 'Language label details updated successfully.');
            }
            res.redirect('/general-setting/language-setting')
            

        }catch(err){
            console.log(err);
            res.redirect('/general-setting/language-setting')
        }
    }else{
        res.redirect('/')
    }
    
}

exports.generateLabel = async (req, res) => {

    try{
        const label_data = await labelModel.find().select('vLangLabel vLangTitle');
       
        const filePath = location + '/upload/en.json';

        fs.writeFileSync(filePath, JSON.stringify(label_data, null, 2), 'utf8', (err) => {
            if (err) {
              console.error('Error writing JSON file:', err);

            } else {
              console.log('JSON file created successfully.');
            }
        });
        
        req.flash('success', "File generated successfully.");
        res.redirect('/dashboard')
        
    }catch(err){
        console.log(err);
        req.flash('error', "Something went wrong while genrating label.");
        res.redirect('/dashboard')

    }
}

exports.edit = async (req, res) => {
    if (req.session.email) {
        try {
            let data =  await labelModel.find({_id: req.params.id});
            res.render("../view/setting/languageAdd", { is_layout: true, action: 'edit', labelData: data[0]});
        } catch (error) {
            console.error('error: ', error);
            req.flash("error", "An error occurred.");
            res.redirect("/dashboard");
        }
    } else {
        res.redirect('/');
    }
}
