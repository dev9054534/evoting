const voterModel = require("../model/voterModel");
// const GeneralLibrary = require("../library/GeneralLibrary");
const path = require("path");
const fs = require("fs");
const readline = require("readline");
const {mongoose, ObjectId} = require("mongoose");

exports.index = (req, res) => {
    if (req.session.email) {
        try {
            is_layout = true;
            res.render("../view/voter/index", {
                is_layout: is_layout,
            });
        } catch (error) {
            console.error("error: ", error);
        }
    } else {
        res.redirect("/");
    }
}

exports.save_voter_csv = async (req, res) => {
    if (req.session.email) {
        try {
            let csvVoterFile = req.files.csvFile

            if (csvVoterFile.size === 0) {
                throw new Error("File is empty or does not exist...!")
            }

            //--------------------------------------> save csv file to upload folder <--------------------------------------//


            // Define the upload directory and create it if it doesn't exist
            const uploadDir = path.join(__dirname, "../upload/voters");

            if (!fs.existsSync(uploadDir)) {
                fs.mkdirSync(uploadDir);
            }

            // Generate a unique file name using the current timestamp
            const ext = csvVoterFile.mimetype.split("/").pop();
            const fileName = Date.now() + "." + ext;

            const filePath = path.join(uploadDir, fileName);

            await csvVoterFile.mv(filePath);


            console.log("File uploaded successfully");

            //--------------------------------------> insert csv file's data to mangodb <--------------------------------------//

            // Check if filePath is empty or does not exist
            if (!fs.existsSync(filePath) || fs.statSync(filePath).size === 0) {
                throw new Error("File is empty or does not exist...!")
            }

            const stream = fs.createReadStream(filePath);
            // Create a readline interface to read data line by line from the input stream
            const reader = readline.createInterface({ input: stream });
            let csvData = [];

            // Process each line of the CSV file
            reader.on("line", row => {
                csvData.push(row.split(","));
            });

            // After reading the file lines, process the data
            reader.on("close", () => {
                // Check if the CSV data is empty
                if (csvData.length == 0) {
                    throw new Error("File is empty or does not exist...!")
                }

                // Extract the header row and remove it from the data
                const headerRow = csvData[0];
                console.log('headerRow: ', headerRow);
                csvData.shift();

                // // Define the forbidden field names in lowercase
                // const forbiddenFields = ["index", "email", "phone"];

                // // Check if any forbidden fields are present in the headerRow
                // const forbiddenFieldsFound = headerRow.some((field) => {
                //    return forbiddenFields.includes(field.toLowerCase())
                // });
                
                // console.log('forbiddenFieldsFound: ', forbiddenFieldsFound);

                // if (!forbiddenFieldsFound) {
                //     throw new Error("Header row contains forbidden fields.");
                // }

                // Create a dynamic schema based on header columns
                const dynamicSchema = {};
                headerRow.forEach((key) => {
                    dynamicSchema[key] = String;
                });


                // Function to insert data into the database
                async function insertData() {
                    let csvArray = [];
                    const ObjectId = mongoose.Types.ObjectId;

                    for (const item of csvData) {
                        let csvObj = {};
                        headerRow.forEach((key, index) => {
                            csvObj["_id"] = new ObjectId();
                            csvObj[key] = item[index];
                           
                        });
                        
                        const votersDetails = new voterModel({
                            iUserId: req.session.userId,
                            voters: csvObj
                        });
                        await votersDetails.save();
                        csvArray.push(csvObj)
                    }


                    console.log('Data inserted successfully.');
                    req.flash("success", "Csv Upload Sucessfully")
                    res.redirect('/voters');
                }
                insertData();
            });


        } catch (error) {
            console.error("error: ", error.message);
            req.flash("error", error.message)
            res.redirect('/dashboard');
        }

    } else {
        res.redirect("/");
    }
}

exports.ajax_listing = async (req, res) => { 
    try{
        let {vAction, iVoterId} = req.body;

        if (vAction === "delete" && iVoterId != null) {
            await voterModel.findByIdAndDelete(req.body.iVoterId);
          }
  
        if (vAction === "multiple_delete" && iVoterId != null) {
        let voterID = iVoterId.split(",");

        await voterModel.deleteMany({
            _id: {
            $in: voterID,
            },
        });
        }

        //   let voterDetails = await voterModel.aggregate([
        //       {
        //           $match: {}
        //         },
        //         {
        //             $unwind: "$voters"
        //         },
        //         {
        //             $limit: 5
        //         },
        //         {
        //             $project: {
        //                 "voters._id": 1,
        //                 "voters.Index": 1,
        //                 "voters.Email": 1,
        //                 "voters.Phone": 1,
        //             }
        //         }
        //     ])
            let voterDetails = await voterModel.find({iUserId: req.session.userId}).exec();

            res.render("../view/voter/ajax_listing", {
            layout: false,
            voterDetails: voterDetails,
            // paginator: paginator,
            // datalimit: limit,
            // dataCount: dataCount,
            // iAgentId: req.session.userId,
        });
    }catch(err){
        console.log(err)
    }
   
}

exports.add = (req, res) => {
    if (req.session.email) {
        try {
            is_layout = true;
            res.render("../view/voter/add", {
                is_layout: is_layout,
                action: 'add'
            });
        } catch (error) {
            console.error("error: ", error);
        }
    } else {
        res.redirect("/");
    }
}

exports.storeData = async (req, res) =>{

    try{
        
        let votersData = await voterModel.find({iUserId: req.session.userId }).exec();
       
        let status = '';
        let message = ""

        const { Index, Email, Phone } = req.body;
        const data = {
            Index: Index,
            Email: Email,
            Phone: Phone
        }

        if(req.body.action == 'add' && status != 'error'){
            for(let i = 0; i < votersData.length; i++){
               if(votersData[i].voters.Index === data.Index || votersData[i].voters.Email === data.Email){
                status = 'error';
                message = "Id or email already exists."
                res.send({status: status, message: message})
                break;
               }
            }
            if(status == 'error') return;
            const response =  await new voterModel({iUserId: req.session.userId, voters: data}).save();
            if(response){
                status = 'success';
                message = 'Data added successfully.'
            }
        }

        if(req.body.action == 'edit'){
            const response = await voterModel.updateOne({_id: req.body.voterId}, {$set: {voters: data}});
            if(response){
                status = 'success';
                message = 'Data updated successfully.'
            }
        }

        res.send({status: status, message: message})
    }catch(err){
        console.log(err)
    }
}

exports.edit = async (req, res) =>{
   
    try{
        const voterData = await voterModel.find({_id: req.params.id}).exec();
        is_layout = true;
        res.render("../view/voter/add", {
            is_layout: is_layout,
            voterData: voterData[0],
            action: 'edit'
        });
    }catch(err){
        console.log(err)
    }
}