exports.index = (req, res) => {
    if (req.session.email) {
        try {
          is_layout = true;
          res.render("../view/ballot/index", {
            is_layout: is_layout,
          });
        } catch (error) {
          console.log("error: ", error);
        }
      } else {
        res.redirect("/");
      }
}