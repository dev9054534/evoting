const VoterModel = require('../model/voterModel');

exports.index = async (req, res) => {
  if (req.session.email) {
    res.render("../view/election/index.handlebars", {
      is_layout: true,
    });
  } else {
    res.redirect("/");
  }
};

exports.add = async (req, res) => {
  if (req.session.email) {
    is_layout = true;
    res.render("../view/election/add", {
      is_layout: is_layout,
    });
  } else {
    res.redirect("/");
  }
};

exports.add_action = async (req,res) =>{
  
}

exports.getData = async(req, res) => {
  if(req.session.email){
    try{
      const data = await VoterModel.find({iUserId: req.session.userId}).exec();
      res.send({data: data})

    }catch(err){
      console.log(err)
    }
  }else{
    res.redirect('/')
  }
}