const authModel = require('../model/authModel');
const settingModal = require('../model/settingModel');
const Paginator = require("../library/PaginatorLibrary");

exports.index = (req, res) => {
    if (req.session.email) {
        try {
          is_layout = true;
          res.render("../view/organization/index", {
            is_layout: is_layout,
          });
        } catch (error) {
          console.log("error: ", error);
        }
      } else {
        res.redirect("/");
      }
}

exports.ajax_listing = async (req, res) => {

    if (req.session.email) {
      try {
        let { iOrganizationId, vAction, searchInput, vPage } = req.body;
  
        if (
          vAction === "search" ||
          vAction === "delete" ||
          vAction === "multiple_delete"
        ) {
          if (vAction === "delete" && iOrganizationId != null) {
            await authModel.findByIdAndDelete({_id: iOrganizationId});
          }
  
          if (vAction === "multiple_delete" && iOrganizationId != null) {
            let organizationID = iOrganizationId.split(",");
  
            await authModel.deleteMany({
              _id: {
                $in: organizationID,
              },
            });
          }
  
          let searchSQL = {eType: 'organizer'};
          if (searchInput.length > 0) {
            let generalSearch = new RegExp(searchInput, "i");
            searchSQL.$or = [
              // {  : generalSearch },
              // { email: generalSearch },
              // // { iContact: generalSearch },
              // { status: new RegExp(searchInput) },
            ];
          }
  
          // Pagination
          var companyData = await settingModal.findOne().exec();
  
          var dataCount = await authModel.find().count();
          vPage = 1;
          let vItemPerPage = companyData.iItemPerPage || 5;
          let vCount = dataCount;
  
          if (req.body.vPage != "") {
            vPage = req.body.vPage;
          }
  
          let criteria = {
            vPage: vPage,
            vItemPerPage: vItemPerPage,
            vCount: vCount,
          };
  
          let paginator = Paginator.pagination(criteria);
          let start = (vPage - 1) * vItemPerPage;
          let limit = vItemPerPage;
          // End
  
          let organizationData = await authModel.find(searchSQL).skip(start).limit(limit).sort({ _id: "desc" });
  
          res.render("../view/organization/ajax_listing", {
            layout: false,
            organizationData: organizationData,
            paginator: paginator,
            datalimit: limit,
            dataCount: dataCount,
            iAgentId: req.session.userId,
          });
        }
      } catch (error) {
        console.log("error: ", error);
        return res.status(500).send({
          message: "error!",
        });
      }
    } else {
      res.status(401).send("Unauthorized");
    }
  };

exports.add = (req, res) => {
    if(req.session.email){
      let is_layout = true;
      res.render('../view/organization/add',{
        is_layout: is_layout,
        action: 'add'
      })
    }else{
      res.redirect('/')
    }
}

exports.addAction = async (req, res) => {
  try{
      if(req.session.email){
        let {o_name, o_location, o_industry, o_membersize, email, password } = req.body;

        if(req.body.action == 'edit'){

          var pass = await authModel.find({_id: req.body.iOrganizationId}).select('password');
        }

        const org_data = {
          email: email,
          password: req.body.password == '' ? pass.password : password,
          eType: 'organizer',
          organization_details: {
            o_name,
            o_location,
            o_industry,
            o_membersize,
          }
        }

        if(req.body.action == 'add'){
          const result = new authModel(org_data)
          await result.save();
          req.flash('success', 'Organization added successfully.');
        }else{
          await authModel.updateOne({_id: req.body.iOrganizationId}, {$set: org_data});
          req.flash('success', 'Organization details updated successfully.');
        }
       
        res.redirect('/organizations')
      }
  }catch(err){
    console.log(err);
    req.flash('error', "Something went wrong !");
    res.redirect('/organizations')
  }
}

exports.edit = async (req, res) => {
  try{
    if(req.session.email){
      const organizationData = await authModel.find({_id: req.params.id});

      let is_layout = true;
      res.render('../view/organization/add',{
        is_layout: is_layout,
        action: 'edit',
        organizationData:organizationData[0]
      })
    }else{
      res.redirect('/')
    }
  }catch(err){
    console.log(err)
  }
  
}


