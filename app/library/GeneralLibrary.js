// const async = require("hbs/lib/async");
const fs = require('fs');
const path  = require("path")
// const AWS = require('aws-sdk');
// const CompanyDetailModel = require("../model/CompanyDetailModel");
// const RoleModulePermissionModel = require("../model/RoleModulePermissionModel");
// const RoleModuleMasterModel = require("../model/RoleModuleMasterModel");
// const NotificationModel = require("../model/NotificationModel");
// const {
//     ElastiCache
// } = require("aws-sdk");
// const moment = require('moment');
// const axios = require('axios');
// const { google } = require('googleapis');
// const utils = require('../utils/common');

module.exports = {

    // company_information: async function (req, res, next) {
    //     var SQL = {};
    //     var companyData = await CompanyDetailModel.find().lean().exec();
    //     return companyData;
    // },

    // awsFileUpload: async function (fileName, path) {

    //     const s3 = new AWS.S3({
    //         accessKeyId: process.env.AWS_ACCESS_KEY_ID,
    //         secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY,
    //         endpoint: process.env.AWS_URL,
    //         s3ForcePathStyle: true,
    //     });

    //     const fileContent = fileName.data;

    //     const filePath = 'erp/' + path + fileName.name;

    //     const params = {
    //         Bucket: process.env.AWS_BUCKET,
    //         Key: filePath,
    //         Body: fileContent,
    //         Bucket: process.env.AWS_BUCKET,
    //         CreateBucketConfiguration: {
    //             LocationConstraint: process.env.AWS_DEFAULT_REGION
    //         },
    //         ACL: 'public-read',
    //         ContentType: fileName.mimetype
    //     };

    //     const awsURL = await s3.upload(params).promise();

    //     return awsURL.Location;
    // },

    // awsFileRead: async function (fileName, path) {

    //     const s3 = new AWS.S3({
    //         accessKeyId: process.env.AWS_ACCESS_KEY_ID,
    //         secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY,
    //         endpoint: process.env.AWS_URL,
    //         s3ForcePathStyle: true,
    //     });

    //     const filePath = 'erp/' + path + fileName;

    //     const params = {
    //         Bucket: process.env.AWS_BUCKET,
    //         Key: filePath,
    //     };

    //     var data = await s3.getObject(params).promise();
    //     var html = Buffer.from(data.Body).toString('utf8');

    //     return html;
    // },

    // permission: async function (CRITERIA) {
    //     var roleModulePermissionData = await RoleModulePermissionModel.find({
    //         iRoleId: CRITERIA.userType
    //     });
    //     for (let index = 0; index < roleModulePermissionData.length; index++) {
    //         const iModuleMasterId = roleModulePermissionData[index].iRoleModuleMasterId;
    //         var roleModuleMasterData = await RoleModuleMasterModel.findOne({
    //             _id: iModuleMasterId
    //         });
    //         roleModulePermissionData[index].iRoleModuleMasterId = roleModuleMasterData.vModuleName;
    //     }

    //     var data = roleModulePermissionData.find(i => i.iRoleModuleMasterId === CRITERIA.URL[1]);

    //     return data;
    // },

    // sendNotification: async function (CRITERIA) {
    //     var dtAddedDate = moment().format('YYYY-MM-DD HH:mm:ss')
    //     const notificationData = new NotificationModel({
    //         vNotification: CRITERIA.vNotification,
    //         iUserId: CRITERIA.iUserId,
    //         vLink: CRITERIA.vLink,
    //         eUserSend: 'admin',
    //         dtAddedDate: dtAddedDate,
    //         vTimezone: CRITERIA.vTimezone
    //     });
    //     notificationData.save();
    // },

    localFilesUpload: function (CRITERIA) {

        if (CRITERIA.Type === 'officeDocx') {
            let mimetype = utils.mimetype;
            for (const key in mimetype) {
                if (CRITERIA.files.mimetype === key) {
                    // console.log(`${key}: ${mimetype[key]}`);
                    var ext = mimetype[key]
                }
            }
        } else {
            /** File name create */
            var ext = CRITERIA.files.mimetype;
            ext = ext.split("/").at(-1);
            if (ext === "svg+xml") {
                ext = "svg";
            }
        }


        // /** File name create */
        // var ext = CRITERIA.files.mimetype;
        // ext = ext.split("/").at(-1);

        image_name = Date.now() + "." + ext;


        /** Make dir  */
        (process.env.APP_ENV === 'development') ? dir = "../assets/assets/" + CRITERIA.path : dir = "../upload" + CRITERIA.path;

        if (!fs.existsSync(dir)) {
            console.log('dir: ', dir);
            fs.mkdirSync(dir);
        }

        /** Move file */
        const vImage = CRITERIA.files;
        vImage.mv(dir + '/' + image_name);

        return (process.env.APP_ENV === 'development') ? process.env.PUBLIC_URL + CRITERIA.path + '/' + image_name
            : process.env.PUBLIC_URL + 'assets/' + CRITERIA.path + '/' + image_name

    },

    unlinkFile: function (filePath) {
        if (filePath) {
            const path = filePath.split("/").slice(3).join("/");
            dir = "../assets/assets/" + path;
            if (fs.existsSync(dir)) {
                fs.unlinkSync(dir);
            }
        }
    },

    // urlImageDownload: async function (CRITERIA) {
    //     let buffer = await axios.get(CRITERIA.files, {
    //         responseType: 'arraybuffer'
    //     })
    //     let ext = buffer.headers['content-type'];
    //     ext = ext.replace("image/", "")
    //     /** File name create */
    //     image_name = Date.now() + '.' + ext;
    //     /** End name */

    //     /** Make dir  */{
    //     (process.env.APP_ENV === 'development') ? dir = "../assets/assets/" + CRITERIA.path : dir = "../assets/assets/assets/" + CRITERIA.path;}
    //     if (!fs.existsSync(dir)) {
    //         fs.mkdirSync(dir);
    //     }
    //     /** End dir */

    //     /**Call Api to save Image */
    //     let image = await axios.get(CRITERIA.files, {
    //         responseType: 'stream'
    //     })
    //     image.data.pipe(fs.createWriteStream(dir + '/' + image_name));

    //     let image_url;
    //     if (process.env.APP_ENV == 'development') {
    //         image_url = process.env.PUBLIC_URL + CRITERIA.path + '/' + image_name;
    //     } else {
    //         image_url = process.env.PUBLIC_URL + 'assets/' + CRITERIA.path + '/' + image_name;
    //     }

    //     return image_url;
    // },

    // socialAnalyticsDate: async function (date,vTimeZone) {
    //     //this  function is use for shorting dates in analytics
    //     let startDate;
    //     let endDate;
    //     let dateFormat = date;

    //     let currentDate = moment().tz(vTimeZone);
    //     if (dateFormat == "this-month") {
    //       let todayDate = moment().tz(vTimeZone).format('DD');
    //       if(todayDate == 1) {
    //           startDate = moment(currentDate).tz(vTimeZone).startOf("month").format("YYYY-MM-DD");
    //           endDate = moment(currentDate).tz(vTimeZone).format("YYYY-MM-DD");
    //           return { startDate, endDate };
    //       }

    //       startDate = moment(currentDate).tz(vTimeZone).startOf("month").format("YYYY-MM-DD");
    //       endDate = moment(currentDate).tz(vTimeZone).subtract(1,'days').format("YYYY-MM-DD");
    //     }

    //   //   if (dateFormat == "last-month") {
    //   //     // Get the start date of the previous month
    //   //     startDate = moment()
    //   //       .subtract(1, "months")
    //   //       .startOf("month")
    //   //       .format("YYYY-MM-DD");

    //   //     // Get the end date of the previous month
    //   //     endDate = moment()
    //   //       .subtract(1, "months")
    //   //       .endOf("month")
    //   //       .format("YYYY-MM-DD");
    //   //   }

    //   //   if (dateFormat == "this-week") {
    //   //     // Get the start date of the current week
    //   //     startDate = moment().startOf("isoweek").format("YYYY-MM-DD");

    //   //     // Get the end date of the current week
    //   //     endDate = moment().endOf("isoweek").format("YYYY-MM-DD");
    //   //   }

    //   //   if (dateFormat == "last-week") {
    //   //     // Get the start date of the past week
    //   //     startDate = moment()
    //   //       .subtract(7, "days")
    //   //       .startOf("isoweek")
    //   //       .format("YYYY-MM-DD");

    //   //     // Get the end date of the past week
    //   //     endDate = moment()
    //   //       .subtract(7, "days")
    //   //       .endOf("isoweek")
    //   //       .format("YYYY-MM-DD");
    //   //   }
    //     return { startDate, endDate };
    //   },

    //   socialFilesUpload: function (CRITERIA) {
    //       /** File name create */
    //       var ext = CRITERIA.files.mimetype;
    //       ext = ext.split("/").at(-1);

    //       /* File name create */
    //       CRITERIA.i ? image_name = Date.now() + CRITERIA.i + 1+"."+ext : image_name = Date.now()+"."+ext;
    //       /* End name */

    //       /** Make dir  */{
    //       (process.env.APP_ENV === 'development') ? dir = "../assets/assets/" + CRITERIA.path : dir = "../assets/assets/assets/" + CRITERIA.path;}

    //       if (!fs.existsSync(dir)) {
    //           fs.mkdirSync(dir);
    //       }
    //       /* End dir */

    //       /* Move file */
    //       const vImage = CRITERIA.files;
    //       vImage.mv(dir + '/' + image_name);
    //       /* End move */ 

    //       let image_url;
    //       if (process.env.APP_ENV == 'development') {
    //           image_url = process.env.PUBLIC_URL + CRITERIA.path + '/' + image_name;
    //       } else {
    //           image_url = process.env.PUBLIC_URL + 'assets/' + CRITERIA.path + '/' + image_name;
    //       }
    //       return image_url
    //   },

    //   getYtAccessToken: async function (token) {
    //     const oauth2Client = new google.auth.OAuth2(process.env.YT_CLIENT_ID, process.env.YT_CLIENT_SECRET, process.env.YT_REDIRECT_URI);
    //     oauth2Client.setCredentials({
    //         refresh_token: token
    //     });
    //     const res = await oauth2Client.getAccessToken()
    //     return res.token
    // },
}